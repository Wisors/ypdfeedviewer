//
//  YPDAppDelegate.h
//  YandexPhotos
//
//  Created by Wisors on 30/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YPDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic, readonly) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) UIWindow *window;

- (NSURL *)applicationDocumentsDirectory;

@end





//
//  main.m
//  YandexPhotos
//
//  Created by Wisors on 30/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YPDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YPDAppDelegate class]));
    }
}

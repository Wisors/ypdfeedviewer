//
//  YPDPhotoGridViewController.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 05/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "YPDPhotoGridViewController.h"

#import "YPDAppDelegate.h"
#import "YPDCollectionViewCell.h"
#import "YPDImageCacher.h"
#import "YPDFeed.h"
#import "YPDXMLPhotoBuilder.h"
#import "YPDFullImageView.h"

NSString *const kRSSFeedXML = @"http://fotki.yandex.ru/calendar/rss2";

@interface YPDPhotoGridViewController () <NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, YPDFeedFetchDelegate>
//CoreData stuff
@property (nonatomic, strong) NSManagedObjectContext *moc;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
//Handling UICollectionView changes
@property (nonatomic, strong) NSMutableArray *sectionChanges;
@property (nonatomic, strong) NSMutableArray *itemsChanges;
//Model
@property (nonatomic, strong) YPDFeed *feed;
//Views
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) YPDFullImageView *fullImageView;
@end

@implementation YPDPhotoGridViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setSectionInset:UIEdgeInsetsMake(3.0f, 5.0f, 3.0f, 5.0f)];
    [layout setItemSize:CGSizeMake(152.0f, 152.0f)];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [layout setMinimumInteritemSpacing:5.0f];
    [layout setMinimumLineSpacing:5.0f];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    [self.collectionView registerClass:[YPDCollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    [self.view addSubview:self.collectionView];
    
    self.fullImageView = [[YPDFullImageView alloc] initWithFrame:self.view.frame];
    
    self.sectionChanges = [NSMutableArray array];
    self.itemsChanges = [NSMutableArray array];
    
    self.moc = ((YPDAppDelegate *)[[UIApplication sharedApplication] delegate]).managedObjectContext;
    if (self.moc) {
        NSManagedObjectContext *backgroundMoc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        backgroundMoc.parentContext = self.moc;
        
        YPDXMLPhotoBuilder *photoBuilder = [[YPDXMLPhotoBuilder alloc] initWithManagedObjectContex:backgroundMoc];
        NSURL *feedURL = [NSURL URLWithString:kRSSFeedXML];
        self.feed = [[YPDFeed alloc] initWithFeedURL:feedURL andPhotoBuilder:photoBuilder];
        self.feed.delegate = self;
        [self.feed fetchPhotosAsynchronous];
        
        self.refreshControl = [[UIRefreshControl alloc] init];
        [self.refreshControl addTarget:self.feed action:@selector(fetchPhotosAsynchronous) forControlEvents:UIControlEventValueChanged];
        [self.collectionView addSubview:self.refreshControl];
        self.collectionView.alwaysBounceVertical = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.moc) {
        NSError *dbError = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                                      code:1
                                                  userInfo:@{NSLocalizedDescriptionKey : @"Не получилось установить подключение к базе данных."}];
        [self handleError:dbError];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - YPDFeedDelegate implementation

- (void)YPDFeedDidSuccessfullyCompleteFetch:(YPDFeed *)feed
{
    [self.refreshControl endRefreshing];
    if ([self.moc hasChanges]) {
        NSError *dbError;
        [self.moc save:&dbError];
        if (dbError) {
            dbError = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                                 code:2
                                             userInfo:@{NSLocalizedDescriptionKey : @"Не получилось сохранить фотографии в базе данных."}];
            [self handleError:dbError];
        }
    }
}

- (void)YPDFeed:(YPDFeed *)feed didFailWithError:(NSError *)error
{
    [self.refreshControl endRefreshing];
    [self handleError:error];
}

#pragma mark - Error handling

- (void)handleError:(NSError *)error
{
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:_cmd withObject:error waitUntilDone:NO];
        return;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                    message:[error localizedDescription]
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController && self.moc) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([YPDPhotoEntity class])
                                                  inManagedObjectContext:self.moc];
        [fetchRequest setEntity:entity];
        [fetchRequest setFetchBatchSize:10];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdOn.timeIntervalSince1970" ascending:NO];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                    managedObjectContext:self.moc
                                                                                                      sectionNameKeyPath:nil
                                                                                                               cacheName:@"YPDCache"];
        aFetchedResultsController.delegate = self;
        _fetchedResultsController = aFetchedResultsController;
        
        NSError *error = nil;
        if (![self.fetchedResultsController performFetch:&error]) {
            NSError *dbError = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                                          code:1
                                                      userInfo:@{NSLocalizedDescriptionKey : @"Не получилось установить подключение к базе данных."}];
            [self handleError:dbError];
        }
    }
    
    return _fetchedResultsController;
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSMutableDictionary *change = [NSMutableDictionary new];
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = @(sectionIndex);
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = @(sectionIndex);
            break;
    }
    
    [self.sectionChanges addObject:change];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    NSMutableDictionary *change = [NSMutableDictionary new];
    switch(type) {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            change[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [self.itemsChanges addObject:change];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if ([self.sectionChanges count] > 0) {
        [self.collectionView performBatchUpdates:^{
            for (NSDictionary *change in self.sectionChanges) {
                [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                    NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                    switch (type) {
                        case NSFetchedResultsChangeInsert:
                            [self.collectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeDelete:
                            [self.collectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                        case NSFetchedResultsChangeUpdate:
                            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                            break;
                    }
                }];
            }
        }
                                      completion:nil];
    }
    
    if ([self.itemsChanges count] > 0 && [self.sectionChanges count] == 0) {
        if ([self shouldReloadCollectionViewToPreventKnownIssue] || self.collectionView.window == nil) {
            [self.collectionView reloadData];
        }
        else {
            [self.collectionView performBatchUpdates:^{
                for (NSDictionary *change in self.itemsChanges) {
                    [change enumerateKeysAndObjectsUsingBlock:^(NSNumber *key, id obj, BOOL *stop) {
                        NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                        switch (type) {
                            case NSFetchedResultsChangeInsert:
                                [self.collectionView insertItemsAtIndexPaths:@[obj]];
                                break;
                            case NSFetchedResultsChangeDelete:
                                [self.collectionView deleteItemsAtIndexPaths:@[obj]];
                                break;
                            case NSFetchedResultsChangeUpdate:
                                [self.collectionView reloadItemsAtIndexPaths:@[obj]];
                                break;
                            case NSFetchedResultsChangeMove:
                                [self.collectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                                break;
                        }
                    }];
                }
            }
                                          completion:nil];
        }
    }
    
    [self.sectionChanges removeAllObjects];
    [self.itemsChanges removeAllObjects];
}

- (BOOL)shouldReloadCollectionViewToPreventKnownIssue
{
    __block BOOL shouldReload = NO;
    for (NSDictionary *change in self.itemsChanges) {
        [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSFetchedResultsChangeType type = [key unsignedIntegerValue];
            NSIndexPath *indexPath = obj;
            switch (type) {
                case NSFetchedResultsChangeInsert:
                    shouldReload = ([self.collectionView numberOfItemsInSection:indexPath.section] == 0) ? YES : NO;
                    break;
                case NSFetchedResultsChangeDelete:
                    shouldReload = ([self.collectionView numberOfItemsInSection:indexPath.section] == 1) ? YES : NO;
                    break;
                case NSFetchedResultsChangeUpdate:
                    shouldReload = NO;
                    break;
                case NSFetchedResultsChangeMove:
                    shouldReload = NO;
                    break;
            }
            *stop = shouldReload;
        }];
    }
    
    return shouldReload;
}

#pragma mark - UIColletionView delegate && datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YPDCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    YPDPhotoEntity *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [cell setTag:(indexPath.row + 1)];
    [cell.imageView setImage:[UIImage imageNamed:@"image-default"]];
    __weak YPDCollectionViewCell *wCell = cell;
    YPDImageCacheHandler handler = ^(UIImage *image, NSError *error){
        if (!image || error) {
            image = [UIImage imageNamed:@"image-fail"];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (wCell.tag == indexPath.row + 1) {
                [wCell.imageView setImage:image];
            }
        });
    };
    [[YPDImageCacher sharedInstanse] imageForURL:[NSURL URLWithString:photo.thmbURL]
                                 completionBlock:handler];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    YPDPhotoEntity *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    YPDCollectionViewCell *cell = (YPDCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [self.fullImageView setImage:cell.imageView.image];
    [self.fullImageView setAuthor:photo.author];
    [self.fullImageView setTitle:photo.title];
    [self.fullImageView setTag:(indexPath.row + 1)];
    [self.view addSubview:self.fullImageView];
    [self.fullImageView showAnimatedStartFromWindowsFrame:[self.collectionView convertRect:cell.frame toView:nil]];
    
    __weak YPDFullImageView *wFullImage = self.fullImageView;
    YPDImageCacheHandler handler = ^(UIImage *image, NSError *error){
        //При ошибке в ячейке останется миниатюра
        if (image && !error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (wFullImage.tag == indexPath.row + 1) {
                    [wFullImage setImage:image];
                }
            });
        }
    };
    [[YPDImageCacher sharedInstanse] imageForURL:[NSURL URLWithString:photo.fullURL]
                                 completionBlock:handler];
}

@end
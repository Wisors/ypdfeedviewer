//
//  YPDFeed.h
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "YPDPhotoBuilder.h"

/**
 *  Класс работы с удаленным фидом с информацие о изображениях. Для создания фида требуется наличие ссыкли на него, и объект из семейства YPDPhotoBuilder, который знает как обрабатывать информацию, полученную из фида.
 */

@class YPDFeed;

@protocol YPDFeedFetchDelegate <NSObject>
/**
 *  Фид успешно выполнил запрос к серверу.
 *
 *  @param feed Объект фида, посылающего сообщение.
 */
- (void)YPDFeedDidSuccessfullyCompleteFetch:(YPDFeed *)feed;
/**
 *  Фид не смог завершить операцию запроса к серверу и сообщает об ошибке.
 *
 *  @param feed  Объект фида, посылающего сообщение.
 *  @param error Ошибка с описанием проблемы.
 */
- (void)YPDFeed:(YPDFeed *)feed didFailWithError:(NSError *)error;
@end

@interface YPDFeed : NSObject
@property (nonatomic, strong) NSURL *feedURL;
@property (nonatomic, strong) YPDPhotoBuilder *buider;
@property (nonatomic, weak) id <YPDFeedFetchDelegate> delegate;

/**
 *  Конструктор.
 *
 *  @param url     Адрес фида с информацие о изображениях.
 *  @param builder Обработчик информации о изображениях по данному фиду.
 *
 *  @return Объект класса.
 */
- (instancetype)initWithFeedURL:(NSURL *)url andPhotoBuilder:(YPDPhotoBuilder *)builder;

/**
 *  Запустить запрос к фиду асинхронно. Сообщение о завершении будут отправлены делегату фида.
 */
- (void)fetchPhotosAsynchronous;

@end

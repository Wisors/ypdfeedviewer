//
//  YPDPhotoEntity.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "YPDPhotoEntity.h"


@implementation YPDPhotoEntity

@dynamic thmbURL;
@dynamic fullURL;
@dynamic title;
@dynamic author;
@dynamic createdOn;

@end

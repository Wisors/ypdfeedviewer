//
//  YPDPhotoEntity.h
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface YPDPhotoEntity : NSManagedObject

@property (nonatomic, retain) NSString * thmbURL;
@property (nonatomic, retain) NSString * fullURL;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSDate * createdOn;

@end

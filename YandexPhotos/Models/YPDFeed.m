//
//  YPDFeed.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "YPDFeed.h"

#import "NSURL+URLValidation.h"

@interface YPDFeed()
@property (nonatomic, strong) NSURLSessionDataTask *fetchTask;
@end

@implementation YPDFeed

- (instancetype)initWithFeedURL:(NSURL *)url andPhotoBuilder:(YPDPhotoBuilder *)builder;
{
    if ((self = [super init])) {
        _feedURL = url;
        _buider = builder;
    }
    
    return self;
}

- (void)dealloc
{
    [self.fetchTask cancel];
}

- (void)fetchPhotosAsynchronous
{
    [self.fetchTask cancel];
    
    if (![self.feedURL isValidURL]) {
        NSError *addressError = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                                           code:100
                                                       userInfo:@{NSLocalizedDescriptionKey : @"Список фотографий не был загружен. Адрес сервера задан неверно."}];
        [self.delegate YPDFeed:self didFailWithError:addressError];
        return;
    }
    
    if (!self.buider) {
        NSError *addressError = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                                           code:101
                                                       userInfo:@{NSLocalizedDescriptionKey : @"Список фотографий не может быть обработан."}];
        [self.delegate YPDFeed:self didFailWithError:addressError];
        return;
    }
    
    __weak YPDFeed *wSelf = self;
    NSURLSession *session = [NSURLSession sharedSession];
    self.fetchTask = [session dataTaskWithURL:self.feedURL
                            completionHandler:^(NSData *data,
                                                NSURLResponse *response,
                                                NSError *error) {
                                if (error) {
                                    NSError *requestError = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                                                                       code:102
                                                                                   userInfo:@{NSLocalizedDescriptionKey : @"Список фотографий не был загружен. Не удалось установить подключение к серверу."}];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [wSelf.delegate YPDFeed:wSelf didFailWithError:requestError];
                                    });
                                }
                                else {
                                    NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                                    if (httpResp.statusCode == 200) {
                                        [wSelf handlerFeedData:data];
                                    }
                                    else {
                                        NSString *desc = [NSString stringWithFormat:@"Список фотографий не был загружен. Код ответа сервера: %ld", (long)httpResp.statusCode];
                                        NSError *requestError = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                                                                           code:103
                                                                                       userInfo:@{NSLocalizedDescriptionKey : desc}];
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [wSelf.delegate YPDFeed:wSelf didFailWithError:requestError];
                                        });
                                    }
                                }
                            }];
    [self.fetchTask resume];
}

- (void)handlerFeedData:(NSData *)data
{
    __weak YPDFeed *wSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        NSError *error;
        [wSelf.buider insertNewPhotosIntoCoreDatafromData:data error:&error];
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wSelf.delegate YPDFeed:wSelf didFailWithError:error];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wSelf.delegate YPDFeedDidSuccessfullyCompleteFetch:wSelf];
            });
        }
    });
}

@end

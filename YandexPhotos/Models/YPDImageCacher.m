//
//  YPDImageCacher.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 03/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "YPDImageCacher.h"

@interface YPDImageCacher()
@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@property (nonatomic, strong) NSCache *cache;

/**
 *  Директория для хранения кэша на диске.
 *
 *  @return Директория для хранения кэша на диске.
 */
+ (NSString *)cacheDirectoryPath;

/**
 *  Полный путь к изображению на диске, полученный исходя из его ключа.
 *
 *  @param cacheKey Ключ изображения для хранения в кэше.
 *
 *  @return Строка с путем к изображению, либо nil - если ключ nil или пустой.
 */
+ (NSString *)cacheFilePathForCacheKey:(NSString *)cacheKey;

/**
 *  Попытаться достать изображение только из локального кэша по ключу.
 *
 *  @param url Ключ изображения для хранения в кэше.
 *
 *  @return Изображение, если оно найдено в кэше, nil - в противном случае.
 */
- (UIImage *)cachedImageForCacheKey:(NSString *)cacheKey;

/**
 *  Попытаться достать изображение только из дискового кэша по ключу.
 *
 *  @param url Ключ изображения для хранения в кэше.
 *
 *  @return Изображение, если оно найдено на дисковом кэше, nil - в противном случае.
 */
- (UIImage *)loadFromDiskCacheKey:(NSString *)cacheKey;

/**
 *  Сохранить данные в дисковый кэш.
 *
 *  @param imageData Что сохраняем.
 *  @param cacheKey  По какому ключу сохраняем.
 */
- (void)saveImageDataToDisk:(NSData *)imageData forCacheKey:(NSString *)cacheKey;
@end

@implementation YPDImageCacher

+ (NSString *)cacheDirectoryPath
{
    static NSString *__cacheDirectoryPath = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        __cacheDirectoryPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"YPDImageCache"];
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        BOOL directoryExists = [fileManager fileExistsAtPath:__cacheDirectoryPath];
        if (directoryExists == NO) {
            [fileManager createDirectoryAtPath:__cacheDirectoryPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
    });
    
    return __cacheDirectoryPath;
}

+ (NSString *)cacheFilePathForCacheKey:(NSString *)cacheKey
{
    if ([cacheKey length] == 0) return nil;
    NSString *fileName = [NSString stringWithFormat:@"image-%lu", (unsigned long)[cacheKey hash]];
	return [[YPDImageCacher cacheDirectoryPath] stringByAppendingPathComponent:fileName];
}

#pragma mark - Singleton Init

+ (YPDImageCacher *)sharedInstanse
{
    static dispatch_once_t onceToken;
    static YPDImageCacher *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] initUniqueInstance];
    });
    return sharedInstance;
}

- (YPDImageCacher *)initUniqueInstance
{
    if ((self = [super init])) {
        _downloadQueue = [[NSOperationQueue alloc] init];
        _downloadQueue.maxConcurrentOperationCount = 4;
        _cache = [[NSCache alloc] init];
    }
    return self;
}

#pragma mark - Public interface

- (void)imageForURL:(NSURL *)url completionBlock:(YPDImageCacheHandler)hanlder
{
    if (!url) {
        NSError *error = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                                    code:300
                                                userInfo:@{NSLocalizedDescriptionKey : @"Для загрузки изображения необходимо указать ссылку."}];
        if (hanlder) {
            hanlder(nil, error);
        }
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *cacheKey = [url absoluteString];
        UIImage *cachedImage = [self cachedImageForCacheKey:cacheKey];
        if (cachedImage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (hanlder) {
                    hanlder(cachedImage, nil);
                }
            });
        }
        else {
            //Загружаем по ссылке
            [self.downloadQueue addOperationWithBlock:^{
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                NSURLResponse *response;
                NSError *error;
                NSData *imageData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                if (error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (hanlder) {
                            hanlder(nil, error);
                        }
                    });
                    return;
                }
                
                UIImage *image;
                if (imageData) {
                    image = [UIImage imageWithData:imageData];
                }
                
                if (image) {
                    [self.cache setObject:image forKey:cacheKey];
                    [self saveImageDataToDisk:imageData forCacheKey:cacheKey];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (hanlder) {
                            hanlder(image, nil);
                        }
                    });
                }
            }];
        }
    });
}

- (UIImage *)cachedImageForURL:(NSURL *)url
{
    return [self cachedImageForCacheKey:[url absoluteString]];
}

#pragma mark - "Private" methods

- (UIImage *)cachedImageForCacheKey:(NSString *)cacheKey
{
    UIImage *cachedImage = [self.cache objectForKey:cacheKey];
    if (cachedImage) {
        return cachedImage;
    }
    
    UIImage *diskImage = [self loadFromDiskCacheKey:cacheKey];
    if (diskImage) {
        [self.cache setObject:diskImage forKey:cacheKey];
        return diskImage;
    }

    return nil;
}

- (UIImage *)loadFromDiskCacheKey:(NSString *)cacheKey
{
    NSString *cachePath = [YPDImageCacher cacheFilePathForCacheKey:cacheKey];
    if ([cachePath length] == 0) return nil;
    
    NSData *imageData = [NSData dataWithContentsOfFile:cachePath options:0 error:NULL];
    if (imageData) {
        return [[UIImage alloc] initWithData:imageData];
    }
    
    return nil;
}

- (void)saveImageDataToDisk:(NSData *)imageData forCacheKey:(NSString *)cacheKey
{
    NSString *cachePath = [YPDImageCacher cacheFilePathForCacheKey:cacheKey];
    if (!&imageData || [cachePath length] == 0) return;
    
    [imageData writeToFile:cachePath atomically:YES];
}

@end

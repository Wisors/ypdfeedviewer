//
//  YPDXMLPhotoBuilder.h
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "YPDPhotoBuilder.h"

@interface YPDXMLPhotoBuilder : YPDPhotoBuilder

/**
 *  Распознать данные, хранящие информацию о фотографиях в XML формате.
 *
 *  @param data  Исходные данные.
 *  @param error ссылка на ошибку.
 *
 *  @return YES - произошла операция вставки в CoreData, NO - произошла ошибка или новых элементов для вставки не найдено.
 */
- (BOOL)insertNewPhotosIntoCoreDatafromData:(NSData *)data
                                      error:(NSError **)error;
@end

//
//  YPDPhotoBuilder.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "YPDPhotoBuilder.h"

@interface YPDPhotoBuilder()
@property (nonatomic, strong) NSManagedObjectContext *context;
@end

@implementation YPDPhotoBuilder

- (instancetype)init
{
    [super doesNotRecognizeSelector:_cmd];
    return nil;
}

- (instancetype)initWithManagedObjectContex:(NSManagedObjectContext *)context
{
    if ((self = [super init])) {
        _context = context;
    }
    
    return self;
}

- (BOOL)insertNewPhotosIntoCoreDatafromData:(NSData *)data error:(NSError *__autoreleasing *)error
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return NO;
}

- (BOOL)isYPDPhotoWithFullURLalredyExist:(NSString *)fullURL
{
    NSFetchRequest * request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:NSStringFromClass([YPDPhotoEntity class])
                                   inManagedObjectContext:self.context]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"fullURL = %@", fullURL]];
    [request setFetchLimit:1];
    
    NSArray *results = [self.context executeFetchRequest:request error:NULL];
    
    return ([results count] != 0);
}

@end

//
//  YPDXMLPhotoBuilder.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "YPDXMLPhotoBuilder.h"

#import "TBXML.h"
#import "NSString+URLValidation.h"

@interface YPDXMLPhotoBuilder()
@property (nonatomic, strong) NSManagedObjectContext *context;

/**
 *  Получить информацию из XML объекта и если валидный новый объект проивзести вставку в CoreData.
 *
 *  @param item XML структура о фотографии.
 *
 *  @return YES - произведена запись в CoreData, NO - запись не требуется, или данные невалидные.
 */
- (BOOL)insertPhotoIntoCoreDataFromXMLElementIfNeeded:(TBXMLElement *)item;
@end

@implementation YPDXMLPhotoBuilder

- (instancetype)initWithManagedObjectContex:(NSManagedObjectContext *)context
{
    if (context.concurrencyType == NSPrivateQueueConcurrencyType || context.concurrencyType == NSMainQueueConcurrencyType) {
        self = [super initWithManagedObjectContex:context];
    }
    else {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"%@ works only with NSManagedObjectContext of NSPrivateQueueConcurrencyType or NSMainQueueConcurrencyType.", NSStringFromClass([YPDXMLPhotoBuilder class])]
                                     userInfo:nil];
    }
    
    return self;
}

- (BOOL)insertNewPhotosIntoCoreDatafromData:(NSData *)data
                                      error:(NSError **)error;
{
    if (error != NULL && *error) return NO;
    
    NSError *xmlParseError;
    TBXML *tbxml = [TBXML newTBXMLWithXMLData:data error:&xmlParseError];
    if (xmlParseError && error != NULL) {
        *error = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                            code:200
                                        userInfo:@{NSLocalizedDescriptionKey : @"Добавление фотографий в базу данных не может быть осуществлено, так как полученные данные имеют неверный формат."}];
    }
    
    TBXMLElement *channel = [TBXML childElementNamed:@"channel" parentElement:tbxml.rootXMLElement];
    TBXMLElement *item = [TBXML childElementNamed:@"item" parentElement:channel];
    NSMutableArray *array = [NSMutableArray array];
    while (item != NULL) {
        [array addObject:[NSValue valueWithBytes:&item objCType:@encode(TBXMLElement)]];
        item = item->nextSibling;
    }
    
    for (NSValue *value in [array reverseObjectEnumerator]) {
        TBXMLElement *element;
        [value getValue:&element];
        [self insertPhotoIntoCoreDataFromXMLElementIfNeeded:element];
    }
    
    if ([self.context hasChanges]) {
        __block NSError *saveError;
        __block BOOL saved = NO;
        [self.context performBlockAndWait:^{
            saved = [self.context save:&saveError];
        }];
        if (saveError && error != NULL) {
            *error = [[NSError alloc] initWithDomain:YPDErrorDomainName
                                                code:201
                                            userInfo:@{NSLocalizedDescriptionKey : @"Не удалось сохранить новые фотографии в базе данных."}];
        }
        return saved;
    }
    
    return NO;
}

- (BOOL)insertPhotoIntoCoreDataFromXMLElementIfNeeded:(TBXMLElement *)item
{
    TBXMLElement *thumbnailURLElement = [TBXML childElementNamed:@"media:thumbnail" parentElement:item];
    TBXMLElement *fullURLElement = [TBXML childElementNamed:@"media:content" parentElement:item];
    NSString *thmbURL = [TBXML valueOfAttributeNamed:@"url" forElement:thumbnailURLElement];
    NSString *fullURL = [TBXML valueOfAttributeNamed:@"url" forElement:fullURLElement];
    
    if ([fullURL isValidStringURL] && [thmbURL isValidStringURL] && ![self isYPDPhotoWithFullURLalredyExist:fullURL]) {
        YPDPhotoEntity *photo = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([YPDPhotoEntity class])
                                                              inManagedObjectContext:self.context];
        photo.author = [TBXML textForElement:[TBXML childElementNamed:@"author" parentElement:item]];
        photo.title = [TBXML textForElement:[TBXML childElementNamed:@"title" parentElement:item]];
        photo.thmbURL = thmbURL;
        photo.fullURL = fullURL;
        photo.createdOn = [NSDate date];
        return YES;
    }
    
    return NO;
}

@end

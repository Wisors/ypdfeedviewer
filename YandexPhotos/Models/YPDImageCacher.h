//
//  YPDImageCacher.h
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 03/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Singleton класс для кеширования изображений. Имеет три уровня:
 *      1) NSCache - кэш в памяти. Хранит объекты, которые уже были помещены или изъяты из кэша в текущую сессию работы приложения.
 *      2) Дисковый кэш. Хранит все объекты, которые были помещены в кеш.
 *      3) Network. Если объект не найден на первых двух уровнях происходит его загрузка и последующее сохранение на предыдущих уровнях.
 */

/**
 *  Хэндлер завершения операция доступа к изображению.
 *
 *  @param image Объект изображения, либо nil, если объект неудалось изъять.
 *  @param error Описание ошибки о причинах невозможности получить изображение, либо nil в при успешной операции.
 */

typedef void (^YPDImageCacheHandler)(UIImage *image, NSError *error);

@interface YPDImageCacher : NSObject
//Защита от дурака для Singleton
+(instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));

+ (YPDImageCacher *)sharedInstanse;

/**
 *  Асинхронно попытаться достать изображение из кэша (в порядке: память, дисковый кэш, сеть) и выполнить замыкание с результатом операции.
 *
 *  @param url     URL объект служит одновременно идентификатором фотографии в кэше и адресом загрузки изображения по сети.
 *  @param hanlder Замыкание для отправки результатов операции.
 */
- (void)imageForURL:(NSURL *)url completionBlock:(YPDImageCacheHandler)hanlder;

/**
 *  Попытаться достать изображение только из локального кэша по NSURL идентификатору.
 *
 *  @param url URL объект - идентификатор изображения
 *
 *  @return Изображение, если оно найдено в кэше, nil - в противном случае.
 */
- (UIImage *)cachedImageForURL:(NSURL *)url;

@end

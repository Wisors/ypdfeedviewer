//
//  NSURL+URLValidation.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 05/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "NSURL+URLValidation.h"

@implementation NSURL (URLValidation)

- (BOOL)isValidURL
{
    if (self.scheme && self.host) {
        return YES;
    }
    return NO;
}

@end

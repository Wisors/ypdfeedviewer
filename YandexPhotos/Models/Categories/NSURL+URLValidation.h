//
//  NSURL+URLValidation.h
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 05/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (URLValidation)

/**
 *  NSURL валиден?
 *
 *  @return YES - валден, NO - невалиден.
 */
- (BOOL)isValidURL;

@end

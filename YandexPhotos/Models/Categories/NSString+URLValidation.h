//
//  NSString+URLValidation.h
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLValidation)

/**
 *  Строка содержит валидный url?
 *
 *  @return YES - валдиная, NO - если строка пустая или невалидная.
 */
- (BOOL)isValidStringURL;

@end

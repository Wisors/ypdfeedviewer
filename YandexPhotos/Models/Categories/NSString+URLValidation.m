//
//  NSString+URLValidation.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "NSString+URLValidation.h"

#import "NSURL+URLValidation.h"

@implementation NSString (URLValidation)

- (BOOL)isValidStringURL
{
    NSURL *resultURL = [NSURL URLWithString:self];
    if (resultURL && [resultURL isValidURL]) {
        return YES;
    }
    return NO;
}

@end

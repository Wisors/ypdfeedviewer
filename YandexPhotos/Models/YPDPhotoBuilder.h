//
//  YPDPhotoBuilder.h
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 04/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "YPDPhotoEntity.h"

/**
 *  Абстрактный класс, служащий интерфейсом для операции распознания во входящих данных информации о фотографиях и при необходимости ее интеграции в CoreData хранилище. Наследуя данный класс необходимо переопределить метод - (BOOL)insertNewPhotosIntoCoreDatafromData:error:; Все действия с CoreData должны производить в рамках заданного при инициализации контекста.
 */

@interface YPDPhotoBuilder : NSObject

@property (nonatomic, strong, readonly) NSManagedObjectContext *context;

- (instancetype) init __attribute__((unavailable("init not available, -initWithManagedObjectContex: is designated init method")));

- (instancetype)initWithManagedObjectContex:(NSManagedObjectContext *)context;

- (BOOL)insertNewPhotosIntoCoreDatafromData:(NSData *)data
                                      error:(NSError **)error;

/**
 *  Существует ли в CoreData информация о фотографии по ссылке?
 *
 *  @param fullURL Строковая ссылка на фотографию, служащая ее идентификатором.
 *
 *  @return YES - фотография существует, NO - фотография не найдена.
 */
- (BOOL)isYPDPhotoWithFullURLalredyExist:(NSString *)fullURL;

@end

//
//  YPDFullImageView.h
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 03/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  View для просмотра фотографии с описанием. Возможен для вариант появления:
 *      1) Неанимированный - добавление вида через addSubview;
 *      2) Анимированный - добавление вида через addSubview и последующий вызов -(void)showAnimatedStartFromWindowsFrame:
 */

@interface YPDFullImageView : UIView

- (void)setAuthor:(NSString *)author;
- (void)setTitle:(NSString *)title;
- (void)setImage:(UIImage *)image;

/**
 *  Анимировать появление вида со стартовым фреймом в координатах window. Анимация масштабирует картинку из начального фрейма в максимально возможный фрейм в рамках текущих размеров bounds, одновременно затеняя фон и отображая описание (автора и заголовок). Если использовать данный метод для появления, то закрытие будет происходить с обратной анимацией сворачивания вида в стартовый фрейм.
 *  @warning Перед вызовом мотода нужно добавить вид в качестве subview.
 *
 *  @param frame CGRect начальный фрейм анимации.
 */
- (void)showAnimatedStartFromWindowsFrame:(CGRect)frame;

@end

//
//  YPDFullImageView.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 03/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "YPDFullImageView.h"

CGFloat const kDescriptionBlockHeight = 60.0f;
CGFloat const kLabelsRightOffset = 10.0f;
CGFloat const kBackgroundShadowAlpha = 0.8f;
NSTimeInterval const kAnimationTimeInterval = 0.25f;

@interface YPDFullImageView() <UIScrollViewDelegate>
//Subviews
@property (nonatomic, strong) UIView *backgroundShadow;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIView *infoContainer;
@property (nonatomic, strong) UILabel *authorLabel;
@property (nonatomic, strong) UILabel *titleLabel;
//Close gesture recognizer
@property (nonatomic, strong) UITapGestureRecognizer *hideTapRecognizer;
//Animation handling
@property (nonatomic, assign) CGRect startAnimationFrame;
@property (nonatomic, assign) BOOL isAnimating;
@property (nonatomic, strong) UIImage *tmpImage;
@end

@implementation YPDFullImageView

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        self.contentMode = UIViewContentModeScaleAspectFit;
        
        _backgroundShadow = [[UIView alloc] initWithFrame:self.bounds];
        [_backgroundShadow setBackgroundColor:[UIColor blackColor]];
        [_backgroundShadow setAlpha:kBackgroundShadowAlpha];

        _imageView = [[UIImageView alloc] initWithFrame:[self imageViewMaxFrame]];
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        [_imageView setClipsToBounds:YES];
        
        _infoContainer = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - kDescriptionBlockHeight,
                                                                  frame.size.width, kDescriptionBlockHeight)];
        
        _authorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width - kLabelsRightOffset,
                                                                 kDescriptionBlockHeight * 1.0f/3.0f)];
        [_authorLabel setTextAlignment:NSTextAlignmentRight];
        [_authorLabel setTextColor:[UIColor whiteColor]];
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,_authorLabel.frame.size.height,
                                                                frame.size.width - kLabelsRightOffset, kDescriptionBlockHeight * 2.0f/3.0f)];
        [_titleLabel setTextAlignment:NSTextAlignmentRight];
        [_titleLabel setTextColor:[UIColor whiteColor]];
        [_titleLabel setFont:[_titleLabel.font fontWithSize:12]];
        [_titleLabel setNumberOfLines:2];
        [_titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        
        [_infoContainer addSubview:_authorLabel];
        [_infoContainer addSubview:_titleLabel];
        
        [self addSubview:_backgroundShadow];
        [self addSubview:_infoContainer];
        [self addSubview:_imageView];
        
        UITapGestureRecognizer *closeRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCloseTap)];
        [self addGestureRecognizer:closeRecognizer];
    }
    
    return self;
}

- (void)setAuthor:(NSString *)author
{
    if ([author length] != 0) {
        author = [author stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                 withString:[[author substringToIndex:1] capitalizedString]];
        NSDictionary *attribs = @{ NSForegroundColorAttributeName : self.authorLabel.textColor,
                                   NSFontAttributeName : self.authorLabel.font };
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:author attributes:attribs];
        [attributedText setAttributes:@{NSForegroundColorAttributeName : [UIColor redColor]}
                                range:NSMakeRange(0, 1)];
        [self.authorLabel setAttributedText:attributedText];
    }
}

- (void)setTitle:(NSString *)title
{
    [self.titleLabel setText:title];
}

//Для того, чтобы избежать глючной анимации, установку изображения надо производить после ее завершения.
- (void)setImage:(UIImage *)image
{
    if (self.isAnimating) {
        self.tmpImage = image;
    }
    else {
        self.imageView.image = image;
        [self adjustImageFrameToImageSize];
    }
}

- (void)adjustImageFrameToImageSize
{
    CGRect finalFrame = [self imageViewMaxFrame];
    
    CGFloat ratioImage = self.imageView.image.size.width / self.imageView.image.size.height;
    CGFloat ratioFrame = CGRectGetWidth(finalFrame) / CGRectGetHeight(finalFrame);
    
    if (ratioImage >= ratioFrame) {
        CGFloat height = CGRectGetWidth(finalFrame) / ratioImage;
        finalFrame = CGRectMake(0, (CGRectGetHeight(finalFrame) - height)/2, CGRectGetWidth(finalFrame), height);
    }
    else {
        CGFloat width = CGRectGetHeight(finalFrame) * ratioImage;
        finalFrame = CGRectMake((CGRectGetWidth(finalFrame) - width)/2, 0, width, CGRectGetHeight(finalFrame));
    }
    [self.imageView setFrame:finalFrame];
}

#pragma mark - "Private" methods

- (CGRect)imageViewMaxFrame
{
    return CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height - kDescriptionBlockHeight);
}

- (void)handleCloseTap
{
    if (CGRectIsEmpty(self.startAnimationFrame)) {
        [self prepareToClose];
        [self removeFromSuperview];
        return;
    }
    self.isAnimating = YES;
    [UIView animateWithDuration:kAnimationTimeInterval
                     animations:^{
                         [self.imageView setFrame:self.startAnimationFrame];
                         [self.backgroundShadow setAlpha:0.0f];
                         [self.authorLabel setAlpha:0.0f];
                         [self.titleLabel setAlpha:0.0f];
                     } completion:^(BOOL finished) {
                         [self prepareToClose];
                         [self removeFromSuperview];
                     }];
}

- (void)prepareToClose
{
    [self.imageView setImage:nil];
    [self.authorLabel setAttributedText:nil];
    [self.titleLabel setText:@""];
    [self.backgroundShadow setAlpha:kBackgroundShadowAlpha];
    [self.authorLabel setAlpha:1.0f];
    [self.titleLabel setAlpha:1.0f];
    self.isAnimating = NO;
    self.tmpImage = nil;
    self.startAnimationFrame = CGRectZero;
}

- (void)showAnimatedStartFromWindowsFrame:(CGRect)frame
{
    self.startAnimationFrame = [self convertRect:frame fromView:nil];
    CGRect finalFrame = self.imageView.frame;
    [self.imageView setFrame:self.startAnimationFrame];
    [self.backgroundShadow setAlpha:0.0f];
    [self.authorLabel setAlpha:0.0f];
    [self.titleLabel setAlpha:0.0f];
    self.isAnimating = YES;
    [UIView animateWithDuration:kAnimationTimeInterval
                     animations:^{
                         [self.imageView setFrame:finalFrame];
                         [self.backgroundShadow setAlpha:kBackgroundShadowAlpha];
                         [self.authorLabel setAlpha:1.0f];
                         [self.titleLabel setAlpha:1.0f];
                     } completion:^(BOOL finished) {
                         self.isAnimating = NO;
                         if (self.tmpImage) {
                             [self.imageView setImage:self.tmpImage];
                             [self adjustImageFrameToImageSize];
                             self.tmpImage = nil;
                         }
                     }];
}

@end

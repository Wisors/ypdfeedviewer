//
//  YPDCollectionViewCell.h
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 03/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YPDCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;

@end

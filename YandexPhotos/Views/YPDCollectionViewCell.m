//
//  YPDCollectionViewCell.m
//  YandexPhotos
//
//  Created by Alexandr Nikishin on 03/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "YPDCollectionViewCell.h"

@implementation YPDCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self.imageView setClipsToBounds:YES];
        
        [self.contentView addSubview:self.imageView];
    }
    return self;
}

@end
